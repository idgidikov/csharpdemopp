﻿using System;

// Base class for accounts
public abstract class Account
{
    public string Client { get; set; }
    public decimal Balance { get; set; }
    public decimal MonthlyInterestRate { get; set; }

    public Account(string client, decimal balance, decimal monthlyInterestRate)
    {
        Client = client;
        Balance = balance;
        MonthlyInterestRate = monthlyInterestRate;
    }

    public abstract decimal CalculateInterest(int months);
}

// Deposit account
public class DepositAccount : Account
{
    public DepositAccount(string client, decimal balance, decimal monthlyInterestRate)
        : base(client, balance, monthlyInterestRate) { }

    public override decimal CalculateInterest(int months)
    {
        return (Balance < 1000) ? months * MonthlyInterestRate : 0;
    }

    public void Deposit(decimal amount)
    {
        Balance += amount;
    }

    public void Withdraw(decimal amount)
    {
        if (amount <= Balance)
            Balance -= amount;
        else
            Console.WriteLine("Insufficient funds");
    }
}

// Credit account
public class CreditAccount : Account
{
    public CreditAccount(string client, decimal balance, decimal monthlyInterestRate)
        : base(client, balance, monthlyInterestRate) { }

    public override decimal CalculateInterest(int months)
    {
        if (Client is IndividualClient && months <= 3)
            return 0;
        else if (Client is BusinessClient && months <= 2)
            return 0;
        else
            return months * MonthlyInterestRate;
    }

    public void Deposit(decimal amount)
    {
        Console.WriteLine("Cannot deposit to a credit account.");
    }
    public void Withdraw(decimal amount)
    {
        Console.WriteLine("Cannot withdraw from a credit account directly.");
    }
}

// Mortgage account
public class MortgageAccount : Account
{
    public MortgageAccount(string client, decimal balance, decimal monthlyInterestRate)
        : base(client, balance, monthlyInterestRate) { }

    public override decimal CalculateInterest(int months)
    {
        if (Client is IndividualClient && months <= 6)
            return 0;
        else if (Client is BusinessClient && months <= 12)
            return 0.5m * months * MonthlyInterestRate;
        else
            return months * MonthlyInterestRate;
    }

    public void Deposit(decimal amount)
    {
        Console.WriteLine("Cannot deposit to a mortgage account.");
    }
    public void Withdraw(decimal amount)
    {
        Console.WriteLine("Cannot withdraw from a mortgage account directly.");
    }
}

// Clients
public class Client { public string Name { get; set; } }
public class IndividualClient : Client { }
public class BusinessClient : Client { }

// Example usage
class Program
{
    static void Main()
    {
        var individualClient = new IndividualClient { Name = "John Doe" };
        var businessClient = new BusinessClient { Name = "ABC Company" };

        var depositAccount = new DepositAccount(individualClient.Name, 500, 0.02m);
        var creditAccount = new CreditAccount(businessClient.Name, -1000, 0.03m);
        var mortgageAccount = new MortgageAccount(individualClient.Name, -200000, 0.01m);

        Console.WriteLine(depositAccount.CalculateInterest(3));
        Console.WriteLine(creditAccount.CalculateInterest(2));
        Console.WriteLine(mortgageAccount.CalculateInterest(8));

        depositAccount.Deposit(200);
        depositAccount.Withdraw(50);
        creditAccount.Deposit(300);
        creditAccount.Withdraw(200);
        mortgageAccount.Deposit(1000);
        mortgageAccount.Withdraw(500);
    }
}
