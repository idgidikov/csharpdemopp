﻿using System;
using System.Collections.Generic;

public class Shape
{
    public double Width { get; }
    public double Height { get; }

    public Shape(double width, double height)
    {
        Width = width;
        Height = height;
    }

    public virtual double CalculateSurface()
    {
        return 0;
    }
}

public class Triangle : Shape
{
    public Triangle(double width, double height) : base(width, height) { }

    public override double CalculateSurface()
    {
        return Width * Height / 2;
    }
}

public class Rectangle : Shape
{
    public Rectangle(double width, double height) : base(width, height) { }

    public override double CalculateSurface()
    {
        return Width * Height;
    }
}

public class Circle : Shape
{
    public Circle(double radius) : base(radius, radius) { }

    public override double CalculateSurface()
    {
        return Math.PI * Math.Pow(Width, 2);
    }
}

class Program
{
    static void Main()
    {
        // Creating an array of different shapes
        //Shape[] shapes = new Shape[]
        List<Shape> shapes = new List<Shape>
        {
            new Triangle(4, 5),
            new Rectangle(3, 8),
            new Circle(6)
        };

        // Calculating the total surface area
        double totalSurface = 0;
        foreach (var shape in shapes)
        {
            totalSurface += shape.CalculateSurface();
        }

        Console.WriteLine($"Total surface area of all shapes: {totalSurface}");
    }
}
