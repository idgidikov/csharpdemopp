﻿using System;

class Program
{
    static void Main()
    {
        // Initialize an array of 10 employees
        Employee[] employees = new Employee[]
        {
            new Employee("Name1", "LastName1", 5000),
            new Employee("Name2", "LastName2", 6000),
            new Employee("Name3", "LastName3", 4500),
            new Employee("Name4", "LastName4", 7000),
            new Employee("Name5", "LastName5", 5500),
            new Employee("Name6", "LastName6", 8000),
            new Employee("Name7", "LastName7", 4000),
            new Employee("Name8", "LastName8", 7500),
            new Employee("Name9", "LastName9", 4800),
            new Employee("Name10", "LastName10", 6200),
        };

        // Sort employees by salary in descending order
        Array.Sort(employees, (x, y) => y.Salary.CompareTo(x.Salary));

        // Display sorted employees
        Console.WriteLine("Sorted employees by salary in descending order:");
        foreach (var employee in employees)
        {
            Console.WriteLine($"{employee.FirstName} {employee.LastName} - Salary: {employee.Salary}");
        }
    }
}

// Define the Employee class
class Employee
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public decimal Salary { get; set; }

    public Employee(string firstName, string lastName, decimal salary)
    {
        FirstName = firstName;
        LastName = lastName;
        Salary = salary;
    }
}
