﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Person
{
     public class Worker : Human
    {
        public decimal HourlyRate { get; set; }
        public int HoursWorked { get; set; }

        public Worker(string firstName, string lastName, decimal hourlyRate, int hoursWorked)
            : base(firstName, lastName)
        {
            HourlyRate = hourlyRate;
            HoursWorked = hoursWorked;
        }

        public decimal CalculateHourlyWage()
        {
            return HourlyRate * HoursWorked;
        }
    }
}
