﻿using Person;
class Program
{
    static void Main()
    {
        // Пример за използване на класовете
        Student student = new Student("Иван", "Иванов", 5);
        Console.WriteLine($"Студент: {student.FirstName} {student.LastName}, Оценка: {student.Grade}");

        Worker worker = new Worker("Петър", "Петров", 10.0m, 40);
        Console.WriteLine($"Работник: {worker.FirstName} {worker.LastName}, Надница: ${worker.HourlyRate}/час, " +
                          $"Изработени часове: {worker.HoursWorked}, Обща надница: ${worker.CalculateHourlyWage()}");
    }
}