﻿using System;

class Student : IComparable<Student>
{
    public string Name { get; set; }
    public int Grade { get; set; }

    public int CompareTo(Student other)
    {
        // Сравняваме студентите по оценка в нарастващ ред
        return this.Grade.CompareTo(other.Grade);
    }
}

class Program
{
    static void Main()
    {
        // Създаване на масив от студенти
        Student[] students = new Student[]
        {
            new Student { Name = "Иван", Grade = 85 },
            new Student { Name = "Мария", Grade = 78 },
            new Student { Name = "Петър", Grade = 92 },
            new Student { Name = "Анна", Grade = 64 },
            new Student { Name = "Георги", Grade = 75 },
            new Student { Name = "София", Grade = 88 },
            new Student { Name = "Николай", Grade = 95 },
            new Student { Name = "Елена", Grade = 82 },
            new Student { Name = "Димитър", Grade = 70 },
            new Student { Name = "Катя", Grade = 91 }
        };

        // Сортиране на студентите
        Array.Sort(students, (x, y) => y.CompareTo(x));

        // Извеждане на сортираните студенти
        Console.WriteLine("Сортирани студенти по оценка:");
        foreach (var student in students)
        {
            Console.WriteLine($"Име: {student.Name}, Оценка: {student.Grade}");
        }
    }
}