﻿using System;
using System.Collections.Generic;

class Animal
{
    public string Name { get; }
    public int Age { get; }
    public string Gender { get; }

    public Animal(string name, int age, string gender)
    {
        Name = name;
        Age = age;
        Gender = gender;
    }

    public virtual string MakeSound()
    {
        return "Generic animal sound";
    }
}

class Dog : Animal
{
    public Dog(string name, int age, string gender) : base(name, age, gender) { }

    public override string MakeSound()
    {
        return "Woof";
    }
}

class Frog : Animal
{
    public Frog(string name, int age, string gender) : base(name, age, gender) { }

    public override string MakeSound()
    {
        return "Ribbit";
    }
}

class Cat : Animal
{
    public Cat(string name, int age, string gender) : base(name, age, gender) { }

    public override string MakeSound()
    {
        return "Meow";
    }
}

class Kitten : Cat
{
    public Kitten(string name, int age, string gender) : base(name, age, gender) { }

    public override string MakeSound()
    {
        return "Meow (Kitten)";
    }
}

class Tomcat : Cat
{
    public Tomcat(string name, int age, string gender) : base(name, age, gender) { }

    public override string MakeSound()
    {
        return "Meow (Tomcat)";
    }
}

class Program
{
    static void Main()
    {
        List<Animal> animals = new List<Animal>
        {
            new Dog("Buddy", 3, "Male"),
            new Frog("Freddy", 1, "Male"),
            new Cat("Whiskers", 5, "Female"),
            new Kitten("Mittens", 0.5, "Female"),
            new Tomcat("Garfield", 4, "Male")
        };

        foreach (var animal in animals)
        {
            Console.WriteLine($"Name: {animal.Name}, Age: {animal.Age}, Sound: {animal.MakeSound()}");
        }
    }
}
