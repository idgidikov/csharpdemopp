﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School
{
    public class SchoolClass
    {
        public string ClassId { get; set; }
        public List<Student> Students { get; set; }
        public List<Teacher> Teachers { get; set; }

        public SchoolClass(string classId)
        {
            ClassId = classId;
            Students = new List<Student>();
            Teachers = new List<Teacher>();
        }
    }

}
