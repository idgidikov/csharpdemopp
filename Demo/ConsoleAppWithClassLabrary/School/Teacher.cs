﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School
{
    public class Teacher : Person
    {
        public List<string> Subjects { get; set; }

        public Teacher(string name, List<string> subjects) : base(name)
        {
            Subjects = subjects;
        }
    }
}

