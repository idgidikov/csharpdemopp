﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School
{
    public class Student : Person
    {
        public int StudentNumber { get; set; }

        public Student(string name, int studentNumber) : base(name)
        {
            StudentNumber = studentNumber;
        }
    }
}
