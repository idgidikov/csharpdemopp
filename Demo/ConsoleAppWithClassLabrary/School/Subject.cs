﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School
{
    public class Subject
    {
        public string Name { get; set; }
        public int Hours { get; set; }
        public int Exercises { get; set; }

        public Subject(string name, int hours, int exercises)
        {
            Name = name;
            Hours = hours;
            Exercises = exercises;
        }
    }
}
