﻿using ClassLibrary;
using System;

namespace ConsoleApp
    // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double add = MathOperations.Add(1, 2);
            Console.WriteLine($"1 + 2 = {add}");

            double substarct = MathOperations.Subtract(2, 1);
            Console.WriteLine($"2 - 1 = {substarct}");
        }
    }
}