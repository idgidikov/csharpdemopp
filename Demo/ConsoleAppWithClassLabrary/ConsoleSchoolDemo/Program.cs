﻿using School;
class Program
{
   
    static void Main()
    {
        // Създаване на учители
        Teacher teacher1 = new Teacher("Teacher1", new List<string> { "Math", "Physics" });
        Teacher teacher2 = new Teacher("Teacher2", new List<string> { "Chemistry", "Biology" });

        // Създаване на ученици
        Student student1 = new Student("Student1", 1);
        Student student2 = new Student("Student2", 2);

        // Създаване на предмети
        Subject math = new Subject("Math", 4, 50);
        Subject physics = new Subject("Physics", 3, 40);

        // Създаване на клас и добавяне на учители, ученици и предмети
        SchoolClass schoolClass = new SchoolClass("ClassA");
        schoolClass.Teachers.Add(teacher1);
        schoolClass.Teachers.Add(teacher2);
        schoolClass.Students.Add(student1);
        schoolClass.Students.Add(student2);

        Console.WriteLine("School Class: " + schoolClass.ClassId);
        Console.WriteLine("Teachers: ");
        foreach (var teacher in schoolClass.Teachers)
        {
            Console.WriteLine($"- {teacher.Name}, Subjects: {string.Join(", ", teacher.Subjects)}");
        }

        Console.WriteLine("Students: ");
        foreach (var student in schoolClass.Students)
        {
            Console.WriteLine($"- {student.Name}, Student Number: {student.StudentNumber}");
        }
    }
}